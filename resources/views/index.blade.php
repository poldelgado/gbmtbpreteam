<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>G&B MTB PreTeam</title>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <link rel="stylesheet" href="{{asset('css/styles.css')}}">
</head>
<body>

<div class="row">
    <div class="col-md-12">
        <div class="text-center logo">
            <img src="{{asset('img/logo.jpg')}}" class="" width="300px" alt="logo">
        </div>
    </div>
</div>



<!-- Scripts -->
<script src="{{ asset('js/app.js') }}"></script>

</body>
</html>